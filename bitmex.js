var request = require('request');
var _ = require('underscore');
var crypto = require('crypto');

var Bitmex = function(key, secret) {
  this.key = key;
  this.secret = secret;
  _.bindAll(this);
}

Bitmex.prototype._request = function(method, path, data, signature,key) {
  var timeout = this.timeout;
  var options = {
    url: 'https://testnet.bitmex.com/api/v1/order',
    method: method,
	  body:data,
    headers: {  
    }
  };

  if(method === 'POST') {
  options.headers['content-type'] = 'application/json',
  options.headers['Accept']= 'application/json',
  options.headers['X-Requested-With']= 'XMLHttpRequest',
  // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
  // https://www.bitmex.com/app/apiKeysUsage for more details.
  options.headers['api-expires']= expires,
  options.headers['api-key']= key,
  options.headers['api-signature']= signature 
  }

 
  
request.post(options,
	function(error, response, body) {
		if (error) { console.log(error); }
		console.log(body);
	}
);
  

}


Bitmex.prototype._post = function(market, action, Dat,secret,key,Side) {

	expires = new Date().getTime() + (60 * 1000), // 1 min in the future
	data = {symbol:"XBTUSD",orderQty:Dat['amount'],price:Dat['price'],ordType:"Limit",side:Side};
	var verb = 'POST',
	path = '/api/v1/order';
	// Pre-compute the postBody so we can be sure that we're using *exactly* the same body in the request
	// and in the signature. If you don't do this, you might get differently-sorted keys and blow the signature.
	var postBody = JSON.stringify(data);
	var signature = crypto.createHmac('sha256', secret).update(verb + path + expires + postBody).digest('hex');

	this._request('POST', path, postBody,signature,key);
}

Bitmex.prototype.order  = function(market, amount, price,secret,key,side) {
  this._post(market, side, {
    amount: amount,
    price: price,
  },secret,key,side);
}

module.exports = Bitmex;
