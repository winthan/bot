// Disclaimer
// By using this script, you implicitly agree to the following disclaimer:
// Auto Trading carries a high level of risk to your capital and can result in losses that exceed your.
// The high degree of leverage can work against you as well as for you. 
// This script supplied "as is" and all use is at your own risk.

// -------------------
/// CHANGE LOG  
///
///  V0.1.1 
///		THIS IS FOR BACKTESTING ONLY 

'use strict'

const 	express = require('express'),
		APIs = require('./apikeys'),
		bodyParser = require('body-parser'),
		crypto = require('crypto'),
		async = require('asyncawait/async'),
		await = require('asyncawait/await'),
		fs = require('fs'),
		http = require('http'),
		https = require('https'),
		stream = fs.createWriteStream("backtesting-bot.txt"),
		ROOT_APP_PATH = fs.realpathSync('.'),
		path = ROOT_APP_PATH + '/backtesting-bot.txt';
		 
		var sslOptions = {
 		key: fs.readFileSync('encryption/localhost.key'),
  		cert: fs.readFileSync('encryption/localhost.crt')
		};

		console.log('\x1Bc');
		console.log("\x1b[32m\x1b[0m");
		console.log("\x1b[32m\t██████╗ ██╗   ██╗██╗   ██╗    ███████╗███████╗██╗     ██╗\x1b[0m");     
		console.log("\x1b[32m\t██╔══██╗██║   ██║╚██╗ ██╔╝    ██╔════╝██╔════╝██║     ██║\x1b[0m");    
		console.log("\x1b[32m\t██████╔╝██║   ██║ ╚████╔╝     ███████╗█████╗  ██║     ██║\x1b[0m");     
		console.log("\x1b[32m\t██╔══██╗██║   ██║  ╚██╔╝      ╚════██║██╔══╝  ██║     ██║\x1b[0m");    
		console.log("\x1b[32m\t██████╔╝╚██████╔╝   ██║       ███████║███████╗███████╗███████╗\x1b[0m");
		console.log("\x1b[32m\t╚═════╝  ╚═════╝    ╚═╝       ╚══════╝╚══════╝╚══════╝╚══════╝\x1b[0m");
		console.log("\x1b[32m\tWhoever is greedy for unjust gain troubles his own.\t\x1b[0m");
		console.log("\x1b[32m\t\tBUY SELL PRO Version 0.1.1 by Winthan \t\t\x1b[0m\n");

		console.log("\nThe Backtesting bot is running. please don't exit this. If you wanna stop the bot, you can press CTRL + C or close the window.\n");
console.log('You can find the backtesting trade log at '+ path); 
var app = express();
http.createServer(app).listen(80);
https.createServer(sslOptions, app).listen(443);

app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', function(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed
    res.send('BOT is running.');
});

 

 


  	app.post('/order', function (req, res) {

  	res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed
    	console.log("\n");
		const body = req.body.Body
		res.set('Content-Type', 'text/plain') 


		var order = req.body.order; 
		console.log(order);
		var ordersize  = req.body.size;
		console.log(ordersize);
		var ex = req.body.exchange;
		console.log(ex);
		var pair = req.body.pair;
		var Pair = pair.substring(0,3) + "-" + pair.substring(3);
		console.log(Pair);
		var price = req.body.price;
		console.log(price);

		function getDateTime() {

	    var date = new Date();

	    var hour = date.getHours();
	    hour = (hour < 10 ? "0" : "") + hour;

	    var min  = date.getMinutes();
	    min = (min < 10 ? "0" : "") + min;

	    var sec  = date.getSeconds();
	    sec = (sec < 10 ? "0" : "") + sec;

	    var year = date.getFullYear();

	    var month = date.getMonth() + 1;
	    month = (month < 10 ? "0" : "") + month;

	    var day  = date.getDate();
	    day = (day < 10 ? "0" : "") + day;

	    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;

	};

	  fs.appendFileSync(path, "\n "  + getDateTime() + "  " + ex + " - " + order  + " " + Pair + " @ " + price + " , Size :" + ordersize);
	  res.send('{\"status": \"we got the orders\"}');



});

 





