# Copyright & Disclaimer  #

Copyright (c) 2017 Winthan Aung <winthan@live.com>,
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.


### Info ###

This is just for sample strategy trading bot script to place the 
order via running node js server script to run and listen to post 
request at https://localhost/order. 


### How do I get set up? ###

You must have Node.js on your machine.You can download Node.js at https://nodejs.org/en/download/.

### File Info ###

**Main Files**

	apikeys.js  = This is where you add your API KEYs for real trades. 
	(GDAX - Coinbase, Poloneix, Bitstamp support)

	index.js
	main server script, it will not work without valid API keys 
	in apikeys.js, so you must have updated apikeys.js before 
	you run index.js with node.

	strategy.js  
	The main script to run in Console at Google Chrome Developer tools, Google Chorme Browser. 

	backtesting.js  = testing server script 		
	
**Supporting Files**
	
	package.json   = NodeJS package
	encryption/localhost.crt  = Self Issued Certificate 
	encryption/localhost.key = Self Issued Certificate Key

### How To Run ### 

1 - After you have  all the files in your system, you can run follow command at your terminal on the same directory you stored bot files. 

	npm install 

It may take for a while depend on your internet speed to download neccessary packages for node. 

2 - And later, you can enter following command to have node upto date.  
	
	npm update 

These 1, and 2 are for installing for your bot server js file. You can either run backtesting.js or index.js later for running the server. 

3 - Let's start with testing server script, so you will not lost anything while testing the bot script. You can run by following command:

	node backtesting.js  
	
That will run localhost server. You can visit https://localhost, you may see "BOT is running" on the browser. If you have certificated issues on web browsers,
let it ignore and allow the https://. If you wanna to stop the bot, you can simply close the terminal, and CTRL + C at the terminal where you run the node. 

If you want to run real server, you must update your API keys at apikeys.js file. If you don't know your GDAX API Key, You can generate your GDAX API Key at 
https://www.gdax.com/settings/api with your Coinbase account. You can create your new API key by selection trade check box only. You only need trade API for this bot, don't select others which you 
don't need rather than trading. When you don't need your API, you can remove your API key at GDAX. 

After getting your API Key, please add your key in apikeys.js file and later run by following command :
	
	node index.js 

4 -  Now, you finally know your localhost for bot. It is time to make tradingview strategy tester to turn into bot. 
  

### RUNING THE BOT 
 
You must open strategy.js with any text editor (Emeditor, Sublime Text, or notepad). And Select all the code, and copy it. 
And browse your chart on trading view site, use BUY SELL V2.3 Backtesting indicator on chart. Test your setting for best gain. Once you have the best setting. 

Open Google Developer Tools in Google Chrome Browser, click on Console tab. And paste what you copied from straegy.js and press ENTER. 

You will see The BOT option on the right top then. 

If you don't see bot option on the right, cick on any icons of TOOLS (Watch Lists, Alerts) on the right of the chart. You will see the option.

 - Trade for Profits Only = 
   This is for trading profits only. It will not take any orders even signal say so without profits. It is good for newbies. 
   However, if you have good setting, and wanna take a risk, you may uncheck if you want to trade as whatever signal so say.

 - Order Size  = 
   If you are on ETHUSD pair of Coinbase chart, it will ETH amount, if you want to trade for 5 ETH, you may enter 5 in it.

 - Last Order (BUY) (SELL) = 
   This is for first time traders, if you have already purchased ETH 5, you can simply selected BUY option, and let the bot 
   know you have already bought 5 at which price. So, bot will not BUY again for you or sell without profits as well if you check Trade for Profits only. 
   It will wait until for sell signals to start the bot trading. This option will be update automatically based on how trades take place.

 - Auto Trade Bot  = 
   This option is running BOT on and OFF. If you want to run bot, you can turn on or off if you want to switch off. 

 -  BUY MARKET and SELL MARKET  = 
 	These buttons are for manual trading without bot, if you want to place order without visiting to exchanges, you can simply click on BUY or SELL button. 


So, you are new to this, 

		1 - You can simply Auto Trade Bot to turn on, 
		2 - Click on Trade for Profits Only, to make sure it is trading for profit only. 
		3 - Click on BUY or SELL option and set the previous order, so we can avoid the double orders.
		4 - Set the size of trades, and let the bot makes money for you.
	

NOTE: You must not refresh the page! If you refresh the page, you need to redo again, by pasting the code again on console, and run again. 


### Recommendation

As you know, this is just beta version, so I have add console.log for you, thus I would recommand to open Developer Tools at Google Chrome Browser 
while you are autotrading or setting. At Console tab on Google Developer Tools in Google Chrome Browser, you can see some of message as well when trades are being made.  
you can simply check at console setting to get better view of console log for trades. 

	1 -  hide network
	2 -  Preserve Log
	3 -  Log XMLHttpRequest
	4 - Show Timestamps 
    

Please don't refresh your page while AUTO trade is on. If you don't like auto bot trading, you can simply click buy or sell button for manual trading.
By selecting Auto trading bot option, it will trade automatically for you. So, you just need to open that browser tab and running bot at localhost. 

Once you are family with everything, you can run real trades bot with, after you update API keys in apikeys.js file. 
	
	node index.js  

Wish you all the best!




