// Disclaimer
// By using this script, you implicitly agree to the following disclaimer:
// Auto Trading carries a high level of risk to your capital and can result in losses that exceed your.
// The high degree of leverage can work against you as well as for you. 
// This script supplied "as is" and all use is at your own risk.


'use strict'

const 	express = require('express'),
		APIs = require('./apikeys'),
		bodyParser = require('body-parser'),
		crypto = require('crypto'),
		async = require('asyncawait/async'),
		await = require('asyncawait/await'),
		Gdax = require('gdax'),
		Poloniex = require('poloniex.js'),
		Bitstamp = require('bitstamp-exc'),
		Bittrex = require('node-bittrex-api'),
		Bitmex = require('./bitmex.js'),
		fs = require('fs'),
		http = require('http'),
		https = require('https'),
		stream = fs.createWriteStream("bot.txt"),
		ROOT_APP_PATH = fs.realpathSync('.'),
		path = ROOT_APP_PATH + '/bot.txt',
		apiURI = 'https://api.gdax.com',
		sandboxURI = 'https://api-public.sandbox.gdax.com',
		errmessage = {
		"message":"rejected"
		}; 

	
var 	sslOptions = {
 		key: fs.readFileSync('encryption/localhost.key'),
  		cert: fs.readFileSync('encryption/localhost.crt')
		};



		console.log('\x1Bc');
		console.log("\x1b[32m\x1b[0m");
		console.log("\x1b[32m\t██████╗ ██╗   ██╗██╗   ██╗    ███████╗███████╗██╗     ██╗\x1b[0m");     
		console.log("\x1b[32m\t██╔══██╗██║   ██║╚██╗ ██╔╝    ██╔════╝██╔════╝██║     ██║\x1b[0m");    
		console.log("\x1b[32m\t██████╔╝██║   ██║ ╚████╔╝     ███████╗█████╗  ██║     ██║\x1b[0m");     
		console.log("\x1b[32m\t██╔══██╗██║   ██║  ╚██╔╝      ╚════██║██╔══╝  ██║     ██║\x1b[0m");    
		console.log("\x1b[32m\t██████╔╝╚██████╔╝   ██║       ███████║███████╗███████╗███████╗\x1b[0m");
		console.log("\x1b[32m\t╚═════╝  ╚═════╝    ╚═╝       ╚══════╝╚══════╝╚══════╝╚══════╝\x1b[0m");
		console.log("\x1b[32m\tWhoever is greedy for unjust gain troubles his own.\t\x1b[0m");
		console.log("\x1b[32m\t\tBUY SELL PRO Version 0.1.4 by Winthan \t\t\x1b[0m\n");

		console.log("\nThe bot is running. please don't exit this. If you wanna stop the bot, you can press CTRL + C or close the window.\n");
		console.log('You can find the trade log at '+ path); 
		
		var app = express();
		http.createServer(app).listen(80);
		https.createServer(sslOptions, app).listen(443)

		app.use(bodyParser.urlencoded({ extended: false }))
		app.get('/', function(req, res) {
		    res.setHeader('Access-Control-Allow-Origin', '*');
		    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');  
		    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype');  
		    res.setHeader('Access-Control-Allow-Credentials', true);  
		    res.send('BOT is running.');
		});

 
	  	app.post('/order', function (req, res) {
			res.setHeader('Access-Control-Allow-Origin', '*');
		    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); 
		    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype');  
		    res.setHeader('Access-Control-Allow-Credentials', true);  
		    console.log("\n");
		
				const body = req.body.Body
				res.set('Content-Type', 'text/plain') 
				var order = req.body.order; 
				var ordersize  = req.body.size;
				var ex = req.body.exchange;
				var pair = req.body.pair;
				var Pair = pair.substring(0,3) + "-" + pair.substring(3);
				var price = req.body.price;

				function getDateTime() {
					var date = new Date();
				    var hour = date.getHours();
				    hour = (hour < 10 ? "0" : "") + hour;
				    var min  = date.getMinutes();
				    min = (min < 10 ? "0" : "") + min;
				    var sec  = date.getSeconds();
				    sec = (sec < 10 ? "0" : "") + sec;
				    var year = date.getFullYear();
				    var month = date.getMonth() + 1;
				    month = (month < 10 ? "0" : "") + month;
				    var day  = date.getDate();
				    day = (day < 10 ? "0" : "") + day;
				    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;
				}


				function eGDAX(){

					const publicClient = new Gdax.PublicClient(Pair);

					// apiURI is used for Real Trading, change apiURI to sandboxURI if you are using Sandbox API.
					const authedClient = new Gdax.AuthenticatedClient(APIs.GDAXKey, APIs.GDAXb64secret, APIs.GDAXpassphrase, apiURI);

					var exprice;

				   if (order == 'buy') {
						publicClient.getProductOrderBook({'level': 1}, function(err, response, data){
							var exprice = data.bids[0][0];
							var OrderParams = {
							  'price': exprice,  
							  'size': ordersize,   
							  'product_id': Pair ,
							  'post_only' : true,
							  'time_in_force': 'GTT',
		 					  'cancel_after': 'hour',
							  'type': "limit"
							};
						
							console.log('GDAX - BUY ORDER');
							authedClient.buy(OrderParams, (error, response, data) => {
								
								if (error){
									console.log(error);
									res.send(errmessage);
									return false; 
									}
								else {
										
										const s = JSON.parse(response.body);
										if (s.message === undefined ) {

											if (s.status != "rejected")  {
												console.log('BUY ORDER : OK');
												fs.appendFileSync(path, "\n "  + getDateTime() + "  " + ex + " - BUY " + Pair + " @ " + OrderParams.price + " , Size :" + OrderParams.size) 
												res.send(response.body);
											}

											else {
												console.log('\x1b[31m%s\x1b[0m', "BUY ORDER : REJECTED/FAILED");  
												res.send(errmessage);
											}
											
										}
										else {
											
											console.log('\x1b[31m%s\x1b[0m', "ERROR:" + s.message);  
											res.send(errmessage);


										}

								}

							});
						
						});
				    }
				
					else {
								publicClient.getProductOrderBook({'level': 1}, function(err, response, data){
									var exprice = data.asks[0][0];
									var OrderParams = {
										  'price': exprice,  
										  'size': ordersize,   
										  'product_id': Pair ,
										  'post_only' : true,
										  'time_in_force': 'GTT',
					 					  'cancel_after': 'hour',
										  'type': "limit"
										};
									
										console.log('GDAX - SELL ORDER');
										authedClient.sell(OrderParams, (error, response, data) => {
											if (error){
												console.log(error);
												console.log(response.body);
												res.send(errmessage);
											}
											else {

												const s = JSON.parse(response.body);
												
												if (s.message === undefined ) {

														if (s.status != "rejected")  {
															console.log('SELL ORDER : OK');
															fs.appendFileSync(path, "\n "  + getDateTime() + "  " + ex + " - SELL " + Pair + " @ "  + OrderParams.price + " , Size :" + OrderParams.size);
															res.send(response.body);
														}

														else {
	 														console.log('\x1b[31m%s\x1b[0m', "SELL ORDER : REJECTED/FAILED");
	 														res.send(errmessage); 
														}

												}

												else {
	 												console.log('\x1b[31m%s\x1b[0m', "ERROR:" + s.message);
	 												res.send(errmessage);  

												}
											
											}

										});
								
								});
					}
				}


			function eBITSTAMP(){

				const bitstamp = new Bitstamp({
									    key : APIs.BitStampKey ,
									    secret: APIs.BitStampSecret,
									    clientId : APIs.BitStampClientId,
									    timeout: APIs.BitStampTimeout
									});
				

				switch(order) {
									
									case 'buy':
									console.log('BITSTAMP - BUY ORDER');
									console.log();

									var baseAmount =  ordersize * 100000000 ;
									var limitPrice =  price * 1 ; 
									
									bitstamp.placeTrade(baseAmount, limitPrice ,  pair.substring(0,3),  pair.substring(3), function (err, data) {
									    if (err) {
									        console.log(err);
									        res.send(errmessage);

									    } else {
									        console.log(data);
									        fs.appendFileSync(path, "\n "  + getDateTime() + "  " + ex + " - BUY " + pair + " @ "  + price + " , Size :" + ordersize);
									        res.send(data);

									    }
									});

									break;
									
									case 'sell':
									console.log('BITSTAMP - SELL ORDER');
									var baseAmount =  ordersize * 100000000 ;
									var sbaseAmount = baseAmount  * -1 ; 
									var limitPrice =  price * 1 ; 

									bitstamp.placeTrade(sbaseAmount, limitPrice ,  pair.substring(0,3),  pair.substring(3), function (err, data) {
									    if (err) {
									        console.log(err);
									        res.send(errmessage);
									    } else {
									        console.log(data);
											fs.appendFileSync(path,  "\n "  + getDateTime() + "  " + ex + " - SELL " + Pair + " @ "  + price + " , Size :" + ordersize);
											res.send(data);
									    }
									});

									break;
							}



		}


		function ePolo(){

				let Polo = new Poloniex(APIs.PoloniexKey, APIs.PoloniexSecret); 
				var currencyPair = pair.substring(3) + "_" +  pair.substring(0,3);
				console.log(currencyPair);
				const cmd = {currencyPair, rate: price, amount: ordersize, postOnly: 1}


				switch(order) {
									
									case 'buy':
									console.log('Poloniex - BUY ORDER');
									Polo._private('buy', cmd, function (err, response) {

										if (err) 
											{
 											    console.log('\x1b[31m%s\x1b[0m', "ERROR:" + err.message); 
 											    res.send(errmessage); 
											} 

										else
											{
											    
											    console.log(response);
											    fs.appendFileSync(path, "\n "  + getDateTime() + "  " + ex + " - BUY " + currencyPair + " @ "  + price + " , Size :" + ordersize);
 												res.send(response); 
											}

									});


									 

									break;
									
									case 'sell':
									console.log('Poloniex - SELL ORDER');

									Polo._private('sell', cmd, function (err, response) {

										if (err) 
											{
											    console.log(err.message);
											    res.send(errmessage); 
											} 

										else
											{
											    
											    console.log(response);
											    fs.appendFileSync(path, "\n "  + getDateTime() + "  " + ex + " - SELL " + currencyPair + " @ "  + price + " , Size :" + ordersize);
 												res.send(response);

											  
											}

									});	
								 
									break;
							}



		}


		function eBittrex(){

				Bittrex.options({
  				'apikey' : APIs.BittrexKey,
  				'apisecret' : APIs.BittrexSecret,
  			    'verbose' : true,
  				'cleartext' : false,
  				'baseUrlv2' : 'https://bittrex.com/Api/v2.0',

				});

				switch(order) {
									
									case 'buy':
									console.log('Bittrex - BUY ORDER');

									Bittrex.tradebuy({
									MarketName: Pair,
									OrderType: 'LIMIT',
									Quantity:ordersize,
									Rate: price,
									TimeInEffect: 'GOOD_TIL_CANCELLED', // supported options are 'IMMEDIATE_OR_CANCEL', 'GOOD_TIL_CANCELLED', 'FILL_OR_KILL'
									ConditionType: 'NONE', // supported options are 'NONE', 'GREATER_THAN', 'LESS_THAN'
									Target: 0, // used in conjunction with ConditionType
									}, function( response, err ) {


										if (err) 
											{
											    console.log(err.message);
											    res.send(errmessage); 
											} 


										else
											{
											    
											    console.log(response);
											    fs.appendFileSync(path, "\n "  + getDateTime() + "  " + ex + " - BUY " + Pair + " @ "  + price + " , Size :" + ordersize);
 												res.send(response);									  
											}

 									});

									break;
									
									case 'sell':
									console.log('Bittrex - SELL ORDER');


									Bittrex.tradebuy({
									MarketName: Pair,
									OrderType: 'LIMIT',
									Quantity:ordersize,
									Rate: price,
									TimeInEffect: 'GOOD_TIL_CANCELLED', // supported options are 'IMMEDIATE_OR_CANCEL', 'GOOD_TIL_CANCELLED', 'FILL_OR_KILL'
									ConditionType: 'NONE', // supported options are 'NONE', 'GREATER_THAN', 'LESS_THAN'
									Target: 0, // used in conjunction with ConditionType
									}, function( response, err ) {


										if (err) 
											{
											    console.log(err.message);
											    res.send(errmessage); 
											} 


										else
											{
											    
											    console.log(response);
											    fs.appendFileSync(path, "\n "  + getDateTime() + "  " + ex + " - BUY " + Pair + " @ "  + price + " , Size :" + ordersize);
 												res.send(response);											  
											}

 									});

									break;
							}



		}


		function eBitMex(){

	 			const bitmex = new Bitmex(APIs.BitMexID, APIs.BitMexSecret);
 				price= Math.round(price * 10) / 10; 

				switch(order) {


									/* To Do */ 
									/* Margin Trading Bot */ 

									case 'buy':
									console.log('BitMex - BUY ORDER');
									bitmex.order(pair, ordersize, price, APIs.BitMexSecret, APIs.BitMexID,"Buy");
									fs.appendFileSync(path, "\n "  + getDateTime() + "  " + ex + " - BUY " + pair + " @ "  + price + " , Size :" + ordersize);
									res.send('margin trading - BUY - BitMex');
									break;
									
									case 'sell':
									console.log('BitMex - SELL ORDER');
									bitmex.order(pair, ordersize, price, APIs.BitMexSecret, APIs.BitMexID,"Sell");
									fs.appendFileSync(path, "\n "  + getDateTime() + "  " + ex + " - SELL " + pair + " @ "  + price + " , Size :" + ordersize);
									res.send('margin trading - SELL - BitMex');
									break;




							}



		}


 
		switch (ex) {

			case "coinbase":

			var trading = async (function () {
			
				try {
					var trade = await (eGDAX());
				} 
					catch (ex) {
					console.log('Caught an error');
					console.log(ex);
					}
	
		 	return true;
			});

			trading().then(function (result) {
		//	console.log(result);
			});

			break;

			case "bitstamp":

			var trading = async (function () {
			
				try {
		    		var trade = await (eBITSTAMP());
				} 
					catch (ex) {
					console.log('Caught an error');
					console.log(ex);
					}
	
			return true;
			});

			trading().then(function (result) {
			console.log(result);
			});

			break;
 

 			case "poloniex":

			var trading = async (function () {
			
				try {
		    		var trade = await (ePolo());
				} 
					catch (ex) {
					console.log('Caught an error');
					console.log(ex);
					}
	
			return true;
			});

			trading().then(function (result) {
			console.log(result);
			});

			break;


			case "bittrex":

			var trading = async (function () {
			
				try {
		    		var trade = await (eBittrex());
				} 
					catch (ex) {
					console.log('Caught an error');
					console.log(ex);
					}
	
			return true;
			});

			trading().then(function (result) {
			console.log(result);
			});

			break;

			case "bitmex":

			var trading = async (function () {
			
				try {
		    		var trade = await (eBitMex());
				} 
					catch (ex) {
					console.log('Caught an error');
					console.log(ex);
					}
	
			return true;
			});

			trading().then(function (result) {
			console.log(result);
			});

			break;


			default: 
 			console.log('The exchange is not supported yet.');
			res.send('No Exchanges');

		}				


}) 