// Enter your GDAX API
module.exports.GDAXKey = 'your-key';
module.exports.GDAXb64secret = 'your-secret';
module.exports.GDAXpassphrase = 'your-passphrase';

// Enter Bitstamp API Key 
module.exports.BitStampKey = 'your-key';
module.exports.BitStampSecret = 'your-secret';
module.exports.BitStampClientId = 'your-bitstamp-user-id';
module.exports.BitStampTimeout = 10000;

//Enter Your Poloneix API Key
module.exports.PoloniexKey = '<YOUR Polo API KEY>';
module.exports.PoloniexSecret = '<YOUR Polo SECRET>';

//Enter Your Bittrex API Key
module.exports.BittrexKey = '<Your-Bittrex-Key>';
module.exports.BittrexSecret = '<Your-Bittrex-Secret>';

//Enter Your BitMex API Key
module.exports.BitMexID = '<Your-Bitmex-ID>';
module.exports.BitMexSecret = '<Your-BitMex-Secret>';
